module.exports = {
  printWidth: 100,
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  bracketSameLine: true,
  arrowParens: 'avoid',
  useTabs: false,
  htmlWhitespaceSensitivity: 'ignore',
  endOfLine: 'auto',
  embeddedLanguageFormatting: 'off',
  overrides: [
    {
      files: '*.{scss,css}',
      options: {
        parser: "css",
        singleQuote: false,
        semi: true,
        colorCase: 'upper'
      },
    },
    {
      files: '*.js',
      options: {
        parser: 'babel',
      },
    },
    {
      files: '*.ts',
      options: {
        parser: 'typescript',
        importOrder: [
          '^rxjs(.*)$',
          '^@apps/(.*)$',
          '^[./]',
        ],
        importOrderSeparation: true,
        importOrderSortSpecifiers: true,
        importOrderParserPlugins: [
          'typescript',
          'classProperties',
          'decorators-legacy',
          //'["decorators", { "decoratorsBeforeExport": true }]',
        ],
      },
    },
    {
      files: '*.md',
      options: {
        parser: 'markdown',
      },
    },
    {
      files: '*.json',
      options: {
        parser: 'json',
      },
    },
  ],
};
